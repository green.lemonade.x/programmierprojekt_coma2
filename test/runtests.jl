using MazeGeneration
using Test
using Colors

@testset "node" begin
    node = Node((2,3))
    @test (node.left || node.right || node.up || node.down) == false
    @test node.pos[1] == 2 && node.pos[2] == 3
    node2 = Node((1,2), false, true, true, false)
    @test (!node2.up && node2.left && node2.down && !node2.right)
end

@testset "Visualization" begin
    maze = Maze(reshape([Node((x,y)) for x in 1:2, y in 1:2],(2, 2)))
    maze.visual = MazeViz(maze.nodes, nothing)
    @test maze.visual.dimensions == (2,2)
    @test maze.visual.path == nothing
    @test all(map(wall -> wall.is_wall, maze.visual.walls_vertical))
    @test all(map(wall -> wall.is_wall, maze.visual.walls_horizontal))
    image = create_image(maze.visual)
    @test count(isequal(ARGB32(0.0,0.0,0.0,1.0)), image) == 32
    @test count(isequal(ARGB32(1.0,1.0,1.0,1.0)), image) == 36
end

@testset "MazeGeneration" begin
    count_ways(node::Node) = count(identity, [node.left, node.right, node.up, node.down])

    function test_generation()::Bool
        for _ in 1:100
            m = rand(1:100)
            n = rand(1:100)
            M::Maze = maze(m,n)

            walls = sum(map(count_ways, M.nodes))
            if walls != 2 * (m * n - 1)
                return false
            end
        end
        true
    end

    @test test_generation()
end