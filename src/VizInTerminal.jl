include("MazeSolver.jl")

# Shows it in Terminal
function show_maze(M::Maze)::Nothing
    item = "██"
    nodes = M.nodes
    height = size(nodes)[1]
    width = size(nodes)[2]
    for _ = 1:(width*2+1)
        print(item)
    end
    for y = 1:height
        println()
        print(item)
        for x1 = 0:width - 1
            if !nodes[y + x1 * height].right # RIGHT (down)
                print("  ", item)
            else
                print("    ")
            end 
        end

        println()
        print(item)
        for x2 = 0:width - 1
            if !nodes[y + x2 * height].down # DOWN (right)
                print(item, item)
            else
                print("  ", item)
            end
        end
    end
end

function show_maze(M::Maze, path00::Vector{Node})
    item = "🍺" # wall
    line = "⛹ " #"▒▒"
    strt = "🐝"
    fnsh =  "🦦"
    nodes = M.nodes
    height = size(nodes)[1]
    width = size(nodes)[2]

    path = []
    for i in 1:length(path00)
        append!(path, [path00[i].pos])
    end

    for _ = 1:(width*2+1)
        print(item)
    end
    for y = 1:height
        println()
        print(item)
        for x1 = 0:width - 1
            if !nodes[y + x1 * height].right # RIGHT (down)
                if (y, x1+1) == path[1]
                    print(strt,item)
                elseif (y, x1+1) == path[end]
                    print(fnsh,item)                
                elseif (y, x1+1) in path
                    print(line, item)
                else
                    print("  ", item)
                end
            elseif (y, x1+1) == path[1]
                print(strt,strt)
            elseif (y, x1+1) == path[end]
                print(fnsh,fnsh)   
            elseif (y, x1+1) in path
                print(line,line)
            else
                print("    ")
            end 
        end

        println()
        print(item)
        for x2 = 0:width - 1
            if !nodes[y + x2 * height].down # DOWN (right)
                print(item, item)
            elseif (y, x2+1) == path[1]
                print(strt,item)
            elseif (y, x2+1) == path[end]
                print(fnsh,item)                
            elseif  (y, x2+1) in path
                print(line, item)
            else
                print("  ", item)
            end
        end
    end
end