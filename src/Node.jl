mutable struct Node
    pos::Tuple{Int,Int}
    up::Bool
    left::Bool
    down::Bool
    right::Bool

    function Node(pos::Tuple{Int,Int})::Node
        new(pos, false, false, false, false)
    end

    function Node(pos::Tuple{Int,Int}, up::Bool, left::Bool, down::Bool, right::Bool)::Node
        new(pos, up, left, down, right)
    end
end

#dead code
get_neighbours(node::Node)::Vector{Union{Node, Nothing}} = [node.up, node.left, node.down, node.right]
