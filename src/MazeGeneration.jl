module MazeGeneration
    include("VizInTerminal.jl")
    export Node
    export get_neighbours
    export rotate_positive
    export rotate_negative
    export function_to_test
    export MazeViz
    export create_image
    export Maze
    export show
    export maze
    export maze_solver!
end
