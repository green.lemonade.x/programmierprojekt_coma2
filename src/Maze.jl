# Top left is (1,1)
# x is down, y is right
include("MazeViz.jl")

mutable struct Maze
    nodes::Matrix{Node}
    visual::Union{MazeViz, Nothing}
    path::Union{Vector{Node}, Nothing}

    Maze(nodes::Matrix{Node}) = new(nodes, nothing, nothing)
end

get_node(maze::Maze, idx::Tuple{Int, Int})::Node = maze.nodes[idx[1], idx[2]]
"""
# Fügt Kante hinzu # TRANSPONIERT
function add_edge!(maze::Maze, from::Tuple{Int, Int}, to::Tuple{Int,Int})::Nothing
    if from[1] > to[1]
        get_node(maze, from).left = true
        get_node(maze, to).right = true
    elseif from[1] < to[1]
        get_node(maze, from).right = true
        get_node(maze, to).left = true
    elseif from[2] > to[2]
        get_node(maze, from).up = true
        get_node(maze, to).down = true
    elseif from[2] < to[2]
        get_node(maze, from).down = true
        get_node(maze, to).up = true
    end
    return
end
"""
function add_edge!(maze::Maze, from::Tuple{Int, Int}, to::Tuple{Int,Int})::Nothing
    if from[1] > to[1]
        get_node(maze, from).up = true
        get_node(maze, to).down = true
    elseif from[1] < to[1]
        get_node(maze, from).down = true
        get_node(maze, to).up = true
    elseif from[2] > to[2]
        get_node(maze, from).left = true
        get_node(maze, to).right = true
    elseif from[2] < to[2]
        get_node(maze, from).right = true
        get_node(maze, to).left = true
    end
    return
end

# Entfernt Kante # Keine Garantie für Korrektheit
function remove_edge!(maze::Maze, from::Tuple{Int, Int}, to::Tuple{Int, Int})::Nothing
    if from[1] > to[1]
        get_node(maze, from).left = false
        get_node(maze, to).right = false
    elseif from[1] < to[1]
        get_node(maze, from).right = false
        get_node(maze, to).left = false
    elseif from[2] > to[2]
        get_node(maze, from).up = false
        get_node(maze, to).down = false
    elseif from[2] < to[2]
        get_node(maze, from).down = false
        get_node(maze, to).up = false
    end
end

function Base.show(io::IO,::MIME"text/plain", maze::Maze)
    if maze.visual == nothing
        maze.visual = MazeViz(maze.nodes, maze.path)
    end
    imshow(create_image(maze.visual))
end
