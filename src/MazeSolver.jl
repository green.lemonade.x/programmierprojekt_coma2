include("MazeGenerator.jl")

#Zufälliger Start und Endpunkt
function maze_solver!(maze::Maze)::Vector{Node}
    #Zufälliger Start- und Endknoten
    Start = rand(1:length(maze.nodes))
    End = rand(1:length(maze.nodes))
    return maze_solver!(maze, Start, End)
end

# Beliebiger Start und Endpunkt
function maze_solver!(maze::Maze, Start::(Int), End::Int)::Vector{Node}
    #Wird später zum Maze hinzugefügt
    Path = Vector{Node}()                   #Wird später zum Maze hinzugefügt
    height = size(maze.nodes)[2]
    width = size(maze.nodes)[1]
    #current trackt aktuelle Position des "Verirrten" im Maze
    current = Start
    # Position der rechten Hand, 1 = Up, 2 = Left, 3 = Down, 0 = Right
    Pos_Right_Hand = 2
    # Höre auf wenn der Endknoten gefunden wurde
    while current != End
        #4 Fallunterscheidungen für Position der Hand
        if Pos_Right_Hand == 1
            #Ist Hand an der Wand?
            if !maze.nodes[current].up
                #Darf ich geradeaus weitergehen?
                if maze.nodes[current].left
                    add_node_to_path(Path, maze.nodes[current])
                    #Aktualisierung der Position nach Bewegung im Maze
                    current = current - width
                #Darf ich nach links gehen?
                elseif maze.nodes[current].down
                    add_node_to_path(Path, maze.nodes[current])
                    current = current + 1
                    Pos_Right_Hand = 2
                #Darf ich nach hinten gehen?
                elseif maze.nodes[current].right
                    add_node_to_path(Path, maze.nodes[current])
                    current = current + width
                    Pos_Right_Hand = 3
                #Kann niemals eintreten
                else
                    add_node_to_path(Path, maze.nodes[current])
                    current = current - 1
                end
            else
                #Drehung um 90 Grad im Uhrzeigersinn bzw. nach rechts
                Pos_Right_Hand = (Pos_Right_Hand + 3) % 4
                add_node_to_path(Path, maze.nodes[current])
                current = current - 1
            end
        #3 weitere Fälle, identisch zum oberen Fall, bis auf die Bewegungsrichtungen
        elseif Pos_Right_Hand == 2
            if !maze.nodes[current].left
                if maze.nodes[current].down
                    add_node_to_path(Path, maze.nodes[current])
                    current = current + 1
                elseif maze.nodes[current].right
                    add_node_to_path(Path, maze.nodes[current])
                    current = current + width
                    Pos_Right_Hand = 3
                elseif maze.nodes[current].up
                    add_node_to_path(Path, maze.nodes[current])
                    current = current - 1
                    Pos_Right_Hand = 0
                else 
                    add_node_to_path(Path, maze.nodes[current])
                    current = current - width
                end
            else
                Pos_Right_Hand = Pos_Right_Hand - 1
                add_node_to_path(Path, maze.nodes[current])
                current = current - width
            end
        elseif Pos_Right_Hand == 3
            if !maze.nodes[current].down
                if maze.nodes[current].right
                    add_node_to_path(Path, maze.nodes[current])
                    current = current + width
                elseif maze.nodes[current].up
                    add_node_to_path(Path, maze.nodes[current])
                    current = current - 1
                    Pos_Right_Hand = 0
                elseif maze.nodes[current].left
                    add_node_to_path(Path, maze.nodes[current])
                    current = current - width
                    Pos_Right_Hand = 1
                else 
                    add_node_to_path(Path, maze.nodes[current])
                    current = current + 1
                end
            else
                Pos_Right_Hand = Pos_Right_Hand - 1
                add_node_to_path(Path, maze.nodes[current])
                current = current + 1
            end
        elseif Pos_Right_Hand == 0
            if !maze.nodes[current].right
                if maze.nodes[current].up
                    add_node_to_path(Path, maze.nodes[current])
                    current = current - 1
                elseif maze.nodes[current].left
                    add_node_to_path(Path, maze.nodes[current])
                    current = current - width
                    Pos_Right_Hand = 1
                elseif maze.nodes[current].down
                    add_node_to_path(Path, maze.nodes[current])
                    current = current + 1
                    Pos_Right_Hand = 2
                else 
                    add_node_to_path(Path, maze.nodes[current])
                    current = current + width
                end
            else
                Pos_Right_Hand = (Pos_Right_Hand + 3) % 4
                add_node_to_path(Path, maze.nodes[current])
                current = current + width
            end
        end
    end
    add_node_to_path(Path, maze.nodes[End]) # Fügt Endpunkt hinzu
    maze.path = Path
    maze.visual.path = map(n -> n.pos, Path)
    return Path
end

function add_node_to_path(path::Vector{Node}, node::Node)::Vector{Node}
    #println(path)
    #println(node)
    index = findfirst(n -> n.pos == node.pos, path)
    if index != nothing
        keepat!(path, [1:index;])
    else
        push!(path, node)
    end
    path
end