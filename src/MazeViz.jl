include("Node.jl")
using Colors, Images, ImageView

struct Wall
    is_wall::Bool
    position::Tuple{Int,Int}
end

# Store all the relevant information to construct an image for the maze.
mutable struct MazeViz
    dimensions::Tuple{Int, Int}

    # Store the walls to the right or to the bottom of the node in the corresponding position.
    walls_vertical::Matrix{Wall}
    walls_horizontal::Matrix{Wall}
    path::Union{Vector{Tuple{Int, Int}}, Nothing}

    # Size in pixels
    wall_size::Int
    hall_size::Int
    path_padding::Int
end

function MazeViz(nodes::Matrix{Node}, path::Union{Vector{Node}, Nothing})::MazeViz
    dims = size(nodes)

    # Since the last column (row) is guaranteed to have a wall on the right (down) side, we don't need that last column (row) in the walls matrix.
    walls_vertical = Matrix{Wall}(undef, dims[1], dims[2] - 1)
    walls_horizontal = Matrix{Wall}(undef, dims[1] - 1, dims[2])


    # Iterate over all nodes and generate the corresponding nodes.
    for x=1:dims[1], y=1:dims[2]
        node = nodes[x,y]
        # exclude the last column (row)
        if x < dims[1]
            walls_horizontal[x,y] = Wall(!node.down, (x,y))
        end
        if y < dims[2]
            walls_vertical[x,y] = Wall(!node.right, (x,y))
        end

    end

    # We only need the positions of the path
    path = path == nothing ? nothing : map(n -> n.pos, path)

    MazeViz(dims, walls_vertical, walls_horizontal, path, 1, 3, 1)
end

function create_image(viz::MazeViz)
    # Size of resulting image in pixels.
    dimensions = viz.dimensions .* viz.hall_size .+ (viz.dimensions .+ 1) .* viz.wall_size
    A = ones(ARGB32, dimensions[1], dimensions[2])

    grid_size = viz.wall_size + viz.hall_size

    for wall in viz.walls_vertical
        if !wall.is_wall
            continue
        end
        top_left = ((wall.position[1] - 1) * grid_size, wall.position[2] * grid_size)
        # The vertical wall size is 2 * wall_size + hall_size, the horizontal just wall_size
        bottom_right = (top_left[1] + viz.wall_size + grid_size, top_left[2] + viz.wall_size)
        # +1 for the lower bound because the range is inclusive and indexing starts with 1
        A[(top_left[1] + 1):bottom_right[1], (top_left[2] + 1):bottom_right[2]] .= colorant"gray"
    end

    for wall in viz.walls_horizontal
        if !wall.is_wall
            continue
        end
        top_left = (wall.position[1] * grid_size, (wall.position[2] - 1) * grid_size)
        bottom_right = (top_left[1] + viz.wall_size, top_left[2] + viz.wall_size + grid_size)
        A[(top_left[1] + 1):bottom_right[1],(top_left[2] + 1):bottom_right[2]] .= colorant"gray"
    end

    if viz.path != nothing
        # Make a green dot for the start
        top_left = (viz.path[1] .- 1) .* grid_size .+ (viz.wall_size + viz.path_padding)
        bottom_right = top_left .+  viz.hall_size .-  2 * viz.path_padding
        A[(top_left[1] + 1):bottom_right[1],(top_left[2] + 1):bottom_right[2]] .= colorant"green"

        # A red dot for the rest
        if length(viz.path) > 2
            for pos in viz.path[2:end - 1]
                top_left = (pos .- 1) .* grid_size .+ (viz.wall_size + viz.path_padding)
                bottom_right = top_left .+ viz.hall_size .- 2 * viz.path_padding
                A[(top_left[1] + 1):bottom_right[1],(top_left[2] + 1):bottom_right[2]] .= colorant"red"
            end
        end

        # Make a black dot for the end
        top_left = (viz.path[end] .- 1) .* grid_size .+ (viz.wall_size + viz.path_padding)
        bottom_right = top_left .+  viz.hall_size .- 2 * viz.path_padding
        A[(top_left[1] + 1):bottom_right[1],(top_left[2] + 1):bottom_right[2]] .= colorant"black"
    end

    # Black borders.
    A[1:viz.wall_size, :] .= colorant"black"
    A[(dimensions[1]-viz.wall_size + 1):dimensions[1], :] .= colorant"black"
    A[:, 1:viz.wall_size] .= colorant"black"
    A[:, (dimensions[2]-viz.wall_size + 1):dimensions[2]] .= colorant"black"
    A
end
