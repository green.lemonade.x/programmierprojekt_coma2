# Task 3 #
include("Maze.jl")
using Random

function maze(height, width)::Maze
    M1 = makeMaze(height, width)
    edges = randPath(height, width)
    edgeMaze!(M1, edges)
    #println(M1)
    return M1    
end

#Versuch #3
function randPath(h::Int, w::Int)::Vector{Tuple{Tuple{Int, Int}, Tuple{Int, Int}}}
    #Diesmal beides mit zwei Koordinaten
    found::Vector{Tuple{Int, Int}} = [] # Alle gefundenennenen
    stack::Vector{Tuple{Int, Int}} = [] # Q als Stack
    edges::Vector{Tuple{Tuple{Int, Int}, Tuple{Int, Int}}} = [] # Kanten

    # Zufälliger Startpunkt
    append!(stack, [(rand(1:h), rand(1:w))])
    append!(found, [stack[1]])
    # "Tiefensuche"
    while !isempty(stack)
        v = stack[end]
        direction = shuffle!([1,2,3,4]) # Zufällige Richtung
        none::Bool = true
        # hoch, links, runter, rechts
        coords = [(v[1]+1, v[2]), (v[1], v[2]-1), (v[1]-1, v[2]), (v[1], v[2]+1)]
        for i in direction
            if (isInside(coords[i], h, w)) && !(coords[i] in found)
                append!(stack, [coords[i]])
                append!(found, [coords[i]])
                append!(edges, [(v, coords[i])])
                none = false
                break
            end
        end
        if none
            pop!(stack)
        end     
    end 
    #println(edges)
    return edges
end

# erstellt Maze ohne Kanten in gegebener Dimension
function makeMaze(height::Int, width::Int)::Maze
    nodes = [Node((x,y)) for x in 1:height, y in 1:width]
    return Maze(reshape(nodes, (height, width)))
end

# fügt Kante überall hinzu, wo die Liste edges das sagt.
function edgeMaze!(maze::Maze, edges::Vector{Tuple{Tuple{Int, Int}, Tuple{Int, Int}}})::Nothing
    for i in edges
        add_edge!(maze, i[1], i[2])
    end
end

# Entscheidet, ob aktuelle Position in der Matrix ist
function isInside(pos::Tuple{Int, Int}, height::Int, width::Int)::Bool
    if 0 < pos[1] <= height && 0 < pos[2] <= width
        return true
    else
        return false
    end
end